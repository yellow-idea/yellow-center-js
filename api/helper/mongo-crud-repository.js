"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.list = list;
exports.array = array;
exports.show = show;
exports.count = count;
exports.create = create;
exports.update = update;
exports.softDelete = softDelete;
exports.importData = importData;
exports.appShowSubmit = appShowSubmit;
exports.appSubmit = appSubmit;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _lineNotify = require("./line-notify");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function list() {
  return _list.apply(this, arguments);
}

function _list() {
  _list = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
    var filter,
        projects,
        limit,
        offset,
        sort,
        order,
        _args = arguments;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            filter = _args.length > 0 && _args[0] !== undefined ? _args[0] : {};
            projects = _args.length > 1 && _args[1] !== undefined ? _args[1] : {};
            limit = _args.length > 2 && _args[2] !== undefined ? _args[2] : 10;
            offset = _args.length > 3 && _args[3] !== undefined ? _args[3] : 0;
            sort = _args.length > 4 && _args[4] !== undefined ? _args[4] : "";
            order = _args.length > 5 && _args[5] !== undefined ? _args[5] : "";
            _context.prev = 6;
            _context.next = 9;
            return this.collection.find(filter, projects).skip(parseInt(offset.toString())).limit(limit).toArray();

          case 9:
            _context.t0 = _context.sent;
            _context.next = 12;
            return this.count(filter);

          case 12:
            _context.t1 = _context.sent;
            return _context.abrupt("return", {
              rows: _context.t0,
              total: _context.t1
            });

          case 16:
            _context.prev = 16;
            _context.t2 = _context["catch"](6);
            (0, _lineNotify.errorNotify)("".concat(this.className, " >> list"), "".concat(_context.t2, "\n").concat(JSON.stringify({
              filter: filter,
              offset: offset,
              limit: limit
            })));
            return _context.abrupt("return", {
              rows: [],
              total: 0
            });

          case 20:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[6, 16]]);
  }));
  return _list.apply(this, arguments);
}

function array() {
  return _array.apply(this, arguments);
}

function _array() {
  _array = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2() {
    var filter,
        projects,
        data,
        _args2 = arguments;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            filter = _args2.length > 0 && _args2[0] !== undefined ? _args2[0] : {};
            projects = _args2.length > 1 && _args2[1] !== undefined ? _args2[1] : {};
            _context2.next = 4;
            return this.collection.find(filter, projects).toArray();

          case 4:
            data = _context2.sent;
            _context2.prev = 5;
            return _context2.abrupt("return", {
              data: data
            });

          case 9:
            _context2.prev = 9;
            _context2.t0 = _context2["catch"](5);
            (0, _lineNotify.errorNotify)("".concat(this.className, " >> array"), "".concat(_context2.t0, "\n").concat(JSON.stringify(filter)));
            return _context2.abrupt("return", {
              data: []
            });

          case 13:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, this, [[5, 9]]);
  }));
  return _array.apply(this, arguments);
}

function show() {
  return _show.apply(this, arguments);
}

function _show() {
  _show = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3() {
    var filter,
        projects,
        data,
        _args3 = arguments;
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            filter = _args3.length > 0 && _args3[0] !== undefined ? _args3[0] : {};
            projects = _args3.length > 1 && _args3[1] !== undefined ? _args3[1] : {};
            _context3.next = 4;
            return this.collection.find(filter, projects).toArray();

          case 4:
            data = _context3.sent;
            _context3.prev = 5;
            return _context3.abrupt("return", {
              data: data.length > 0 ? data[0] : null
            });

          case 9:
            _context3.prev = 9;
            _context3.t0 = _context3["catch"](5);
            (0, _lineNotify.errorNotify)("".concat(this.className, " >> show"), "".concat(_context3.t0, "\n").concat(JSON.stringify(filter)));
            return _context3.abrupt("return", {
              data: null
            });

          case 13:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, this, [[5, 9]]);
  }));
  return _show.apply(this, arguments);
}

function count() {
  return _count.apply(this, arguments);
}

function _count() {
  _count = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4() {
    var filter,
        _args4 = arguments;
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            filter = _args4.length > 0 && _args4[0] !== undefined ? _args4[0] : {};
            return _context4.abrupt("return", this.collection.countDocuments(filter));

          case 2:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, this);
  }));
  return _count.apply(this, arguments);
}

function create(_x) {
  return _create.apply(this, arguments);
}

function _create() {
  _create = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(payload) {
    var result;
    return _regenerator["default"].wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.prev = 0;
            _context5.next = 3;
            return this.collection.insertOne(payload);

          case 3:
            result = _context5.sent;
            return _context5.abrupt("return", {
              status_code: "SUCCESS",
              id: result.insertedId
            });

          case 7:
            _context5.prev = 7;
            _context5.t0 = _context5["catch"](0);
            (0, _lineNotify.errorNotify)("".concat(this.className, " >> create"), "".concat(_context5.t0, "\n").concat(JSON.stringify(payload)));
            return _context5.abrupt("return", {
              status_code: "ERROR"
            });

          case 11:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, this, [[0, 7]]);
  }));
  return _create.apply(this, arguments);
}

function update() {
  return _update.apply(this, arguments);
}

function _update() {
  _update = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6() {
    var filter,
        payload,
        result,
        _args6 = arguments;
    return _regenerator["default"].wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            filter = _args6.length > 0 && _args6[0] !== undefined ? _args6[0] : {
              id: null
            };
            payload = _args6.length > 1 ? _args6[1] : undefined;
            _context6.prev = 2;
            _context6.next = 5;
            return this.collection.updateOne(filter, {
              $set: _objectSpread({}, payload)
            });

          case 5:
            result = _context6.sent;

            if (!(result.matchedCount <= 0)) {
              _context6.next = 8;
              break;
            }

            return _context6.abrupt("return", {
              status_code: "NO_DATA"
            });

          case 8:
            return _context6.abrupt("return", {
              status_code: "SUCCESS"
            });

          case 11:
            _context6.prev = 11;
            _context6.t0 = _context6["catch"](2);
            (0, _lineNotify.errorNotify)("".concat(this.className, " >> update"), "".concat(_context6.t0, "\n").concat(JSON.stringify(filter), "\n").concat(JSON.stringify(payload)));
            return _context6.abrupt("return", {
              status_code: "ERROR"
            });

          case 15:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, this, [[2, 11]]);
  }));
  return _update.apply(this, arguments);
}

function softDelete() {
  return _softDelete.apply(this, arguments);
}

function _softDelete() {
  _softDelete = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee7() {
    var filter,
        result,
        _args7 = arguments;
    return _regenerator["default"].wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            filter = _args7.length > 0 && _args7[0] !== undefined ? _args7[0] : {
              _id: null
            };
            _context7.prev = 1;
            _context7.next = 4;
            return this.collection.findOneAndUpdate(filter, {
              $set: {
                deleted_at: new Date()
              }
            });

          case 4:
            result = _context7.sent;

            if (!(result.matchedCount <= 0)) {
              _context7.next = 7;
              break;
            }

            return _context7.abrupt("return", {
              status_code: "NO_DATA"
            });

          case 7:
            return _context7.abrupt("return", {
              status_code: "SUCCESS"
            });

          case 10:
            _context7.prev = 10;
            _context7.t0 = _context7["catch"](1);
            (0, _lineNotify.errorNotify)("".concat(this.className, " >> softDelete"), "".concat(_context7.t0, "\n").concat(JSON.stringify(filter)));
            return _context7.abrupt("return", {
              status_code: "ERROR"
            });

          case 14:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7, this, [[1, 10]]);
  }));
  return _softDelete.apply(this, arguments);
}

function importData(_x2) {
  return _importData.apply(this, arguments);
}

function _importData() {
  _importData = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee8(payload) {
    return _regenerator["default"].wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            _context8.prev = 0;
            _context8.next = 3;
            return this.collection.insertMany(payload);

          case 3:
            return _context8.abrupt("return", {
              status_code: "SUCCESS"
            });

          case 6:
            _context8.prev = 6;
            _context8.t0 = _context8["catch"](0);
            (0, _lineNotify.errorNotify)("".concat(this.className, " >> importData"), "".concat(_context8.t0, "\n").concat(JSON.stringify(payload)));
            return _context8.abrupt("return", {
              status_code: "ERROR"
            });

          case 10:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8, this, [[0, 6]]);
  }));
  return _importData.apply(this, arguments);
}

function appShowSubmit() {
  return _appShowSubmit.apply(this, arguments);
}

function _appShowSubmit() {
  _appShowSubmit = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee9() {
    var filter,
        projects,
        data,
        _args9 = arguments;
    return _regenerator["default"].wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            filter = _args9.length > 0 && _args9[0] !== undefined ? _args9[0] : {};
            projects = _args9.length > 1 && _args9[1] !== undefined ? _args9[1] : {};
            _context9.next = 4;
            return this.collection_app.find(filter, projects).toArray();

          case 4:
            data = _context9.sent;
            _context9.prev = 5;
            return _context9.abrupt("return", {
              data: data.length > 0 ? data[0] : null
            });

          case 9:
            _context9.prev = 9;
            _context9.t0 = _context9["catch"](5);
            (0, _lineNotify.errorNotify)("".concat(this.className, " >> appShowSubmit"), "".concat(_context9.t0, "\n").concat(JSON.stringify(filter)));
            return _context9.abrupt("return", {
              data: null
            });

          case 13:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9, this, [[5, 9]]);
  }));
  return _appShowSubmit.apply(this, arguments);
}

function appSubmit(_x3) {
  return _appSubmit.apply(this, arguments);
}

function _appSubmit() {
  _appSubmit = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee10(payload) {
    var result;
    return _regenerator["default"].wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            _context10.prev = 0;
            _context10.next = 3;
            return this.collection_app.insertOne(payload);

          case 3:
            result = _context10.sent;
            return _context10.abrupt("return", {
              status_code: "SUCCESS",
              id: result.insertedId
            });

          case 7:
            _context10.prev = 7;
            _context10.t0 = _context10["catch"](0);
            (0, _lineNotify.errorNotify)("".concat(this.className, " >> appSubmit"), "".concat(_context10.t0, "\n").concat(JSON.stringify(payload)));
            return _context10.abrupt("return", {
              status_code: "ERROR"
            });

          case 11:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10, this, [[0, 7]]);
  }));
  return _appSubmit.apply(this, arguments);
}