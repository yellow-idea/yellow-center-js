"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = gen_qr_code;

var _qrcode = _interopRequireDefault(require("qrcode"));

function gen_qr_code(text) {
  return new Promise(function (resolve, reject) {
    _qrcode["default"].toDataURL(text, function (err, url) {
      if (err) {
        console.log(err);
        reject(err);
      }

      resolve(url);
    });
  });
}