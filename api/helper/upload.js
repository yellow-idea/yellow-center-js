"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.S3UploadCDN = exports.easyS3Upload = exports.S3Upload = exports.S3Uploader = exports.RemoveFile = exports.UploadImageBase64 = exports.GetFileExtentions = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _fs = _interopRequireDefault(require("fs"));

var _s = _interopRequireDefault(require("aws-sdk/clients/s3"));

var GetFileExtentions = function GetFileExtentions(img64) {
  var data = img64.match(/^data:image\/([a-zA-Z]*);base64,/);
  return data[1];
};

exports.GetFileExtentions = GetFileExtentions;

var UploadImageBase64 = function UploadImageBase64() {
  var img64 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var fileName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
  return new Promise(function (resolve) {
    var base64Data = img64.replace(/^data:image\/([a-zA-Z]*);base64,/, "");

    _fs["default"].writeFile(fileName, base64Data, 'base64', function (err) {
      if (err) {
        console.log(err);
        resolve(false);
      } else {
        resolve(true);
      }
    });
  });
};

exports.UploadImageBase64 = UploadImageBase64;

var RemoveFile = function RemoveFile() {
  var fileName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  return new Promise(function (resolve) {
    _fs["default"].unlink(fileName, function (err) {
      if (err) {
        console.log(err);
        resolve(false);
      } else {
        resolve(true);
      }
    });
  });
};

exports.RemoveFile = RemoveFile;

var S3Uploader = function S3Uploader(options) {
  return new Promise( /*#__PURE__*/function () {
    var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(resolve) {
      var ext, fileName, tmpFile, resUpload, locationUrl;
      return _regenerator["default"].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              if (!(options.img_url === "" || options.img_url.search("http") > -1)) {
                _context.next = 4;
                break;
              }

              resolve(options.img_url);
              _context.next = 20;
              break;

            case 4:
              ext = GetFileExtentions(options.img_url);
              fileName = "".concat(options.fileName, ".").concat(ext);
              tmpFile = "/tmp/".concat(fileName);
              _context.next = 9;
              return UploadImageBase64(options.img_url, tmpFile);

            case 9:
              resUpload = _context.sent;

              if (!resUpload) {
                _context.next = 19;
                break;
              }

              _context.next = 13;
              return S3Upload(options.bucket, "".concat(options.key, "/").concat(fileName), tmpFile);

            case 13:
              locationUrl = _context.sent;
              _context.next = 16;
              return RemoveFile(tmpFile);

            case 16:
              resolve(locationUrl);
              _context.next = 20;
              break;

            case 19:
              resolve(null);

            case 20:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  }());
};

exports.S3Uploader = S3Uploader;

var S3Upload = function S3Upload(bucket, key, tmpFile) {
  return new Promise(function (resolve) {
    var s3 = new _s["default"]({
      region: process.env.SERVICE_REGION,
      accessKeyId: process.env.ACCESS_KEY_ID,
      secretAccessKey: process.env.SECRET_ACCESS_KEY
    });

    var fileStream = _fs["default"].createReadStream(tmpFile);

    fileStream.on('error', function (err) {
      console.log('File Error', err);
      resolve(null);
    });
    var params = {
      Bucket: bucket,
      Key: key,
      Body: fileStream
    };
    s3.putObject(params, function (err, data) {
      if (err) {
        console.log(err, data);
        resolve(null);
      } else {
        resolve("https://s3-".concat(process.env.SERVICE_REGION, ".amazonaws.com/").concat(bucket, "/").concat(key));
      }
    });
  });
};

exports.S3Upload = S3Upload;

var easyS3Upload = function easyS3Upload(key, img_url, fileName) {
  return new Promise( /*#__PURE__*/function () {
    var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(resolve) {
      var img_data;
      return _regenerator["default"].wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return S3Uploader({
                bucket: process.env.S3,
                key: key,
                img_url: img_url,
                fileName: fileName
              });

            case 2:
              img_data = _context2.sent;
              resolve(img_data);

            case 4:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function (_x2) {
      return _ref2.apply(this, arguments);
    };
  }());
};

exports.easyS3Upload = easyS3Upload;

var S3UploadCDN = function S3UploadCDN(imgBase64, key) {
  return new Promise(function (resolve) {
    var a1 = "01000001 01001011 01001001 01000001 01010010 00110101 01000100 00110111 01010000 01010010 00110111 01000100 01010111 00110110 00110110 01011010 01010001 01001101 00110110 01010100";
    var a2 = "01110010 00111000 01111010 01101100 01000111 00111001 01001100 01010011 01110100 01010111 01110010 01101001 01010010 01101001 00110000 01110001 01000101 01011000 01011010 00110101 01101000 01011001 01000101 01000101 01000101 01000110 00110100 01101010 01010000 01101011 01010011 01100111 01100010 01010110 00110110 01101001 01101011 01010100 01000010 01111010 ";
    var accessKeyId = a1.split(" ").map(function (bin) {
      return String.fromCharCode(parseInt(bin, 2));
    }).join("");
    var secretAccessKey = a2.split(" ").map(function (bin) {
      return String.fromCharCode(parseInt(bin, 2));
    }).join("");
    var s3 = new _s["default"]({
      region: "ap-southeast-1",
      accessKeyId: accessKeyId,
      secretAccessKey: secretAccessKey
    });
    var buf = new Buffer(imgBase64.replace(/^data:image\/\w+;base64,/, ""), 'base64');
    var params = {
      Bucket: "cdn.yellow-idea.com",
      Key: key,
      Body: buf
    };
    s3.putObject(params, function (err, data) {
      if (err) {
        console.log(err, data);
        resolve(null);
      } else {
        resolve("https://cdn.yellow-idea.com/".concat(key));
      }
    });
  });
};

exports.S3UploadCDN = S3UploadCDN;