"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.convert_array_to_join_str = exports.replaceID = void 0;

var replaceID = function replaceID(obj) {
  return JSON.parse(JSON.stringify(obj).replace(/"_id"/g, "\"id\""));
};

exports.replaceID = replaceID;

var convert_array_to_join_str = function convert_array_to_join_str(arr, separator, key) {
  var sq = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  var tmp_arr = [];
  var result = "";

  if (arr.length > 0) {
    arr.forEach(function (v) {
      if (sq) {
        tmp_arr.push("'".concat(v[key], "'"));
      } else {
        tmp_arr.push(v[key]);
      }
    });
    result = tmp_arr.join(separator);
  }

  return result;
};

exports.convert_array_to_join_str = convert_array_to_join_str;