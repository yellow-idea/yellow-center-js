"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.internalServerError = exports.notFound = void 0;

// Handle not found errors
var notFound = function notFound(req, res, next) {
  res.status(404).json({
    success: false,
    message: "Requested Resource Not Found (404)"
  }).end();
}; // Handle internal server errors


exports.notFound = notFound;

var internalServerError = function internalServerError(err, req, res, next) {
  res.status(err.status || 500).json({
    message: err.message,
    errors: err
  }).end();
};

exports.internalServerError = internalServerError;