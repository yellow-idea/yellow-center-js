"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MongoConnection = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var MongoClient = require("mongodb").MongoClient;

var MongoConnection = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(url) {
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", new Promise(function (resolve, reject) {
              MongoClient.connect(url, {
                useNewUrlParser: true,
                useUnifiedTopology: true
              }, function (err, client) {
                if (err) {
                  reject(err);
                }

                console.log("Connected successfully to server");
                resolve(client);
              });
            }));

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function MongoConnection(_x) {
    return _ref.apply(this, arguments);
  };
}();

exports.MongoConnection = MongoConnection;