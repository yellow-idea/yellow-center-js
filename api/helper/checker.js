"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isNotNullAndNotEmpty = exports.isHasKey = exports.isObject = exports.isArray = void 0;

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var isArray = function isArray(value) {
  return value && (0, _typeof2["default"])(value) === 'object' && value.constructor === Array;
};

exports.isArray = isArray;

var isObject = function isObject(value) {
  return value && (0, _typeof2["default"])(value) === 'object' && value.constructor === Object;
};

exports.isObject = isObject;

var isHasKey = function isHasKey(obj, key) {
  return Object.keys(obj).find(function (x) {
    return x === key;
  });
};

exports.isHasKey = isHasKey;

var isNotNullAndNotEmpty = function isNotNullAndNotEmpty(obj) {
  return obj ? obj !== "" : false;
};

exports.isNotNullAndNotEmpty = isNotNullAndNotEmpty;