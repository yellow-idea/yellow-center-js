"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.get_created_at = exports.get_date7 = void 0;

Date.prototype.addHours = function (h) {
  this.setHours(this.getHours() + h);
  return this;
};

var get_date7 = function get_date7() {
  return new Date().addHours(7);
};

exports.get_date7 = get_date7;

var get_created_at = function get_created_at() {
  var d = new Date().addHours(7);
  var year = d.getFullYear();
  var month = d.getMonth() + 1;
  var day = d.getDate();
  var hour = d.getHours();
  var min = d.getMinutes();
  var sec = d.getSeconds();
  month = month.toString().length > 1 ? month : "0".concat(month);
  day = day.toString().length > 1 ? day : "0".concat(day);
  hour = hour.toString().length > 1 ? hour : "0".concat(hour);
  min = min.toString().length > 1 ? min : "0".concat(min);
  sec = sec.toString().length > 1 ? sec : "0".concat(sec);

  var _date = [year, month, day].join("-");

  var _time = [hour, min, sec].join(":");

  return "".concat(_date, " ").concat(_time);
};

exports.get_created_at = get_created_at;