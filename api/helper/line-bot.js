"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _lineNotify = require("./line-notify");

var line = require('@line/bot-sdk');

var LineHelper = /*#__PURE__*/function () {
  function LineHelper() {
    (0, _classCallCheck2["default"])(this, LineHelper);
    this.channelAccessToken = process.env.LINE_CHANEL_ACCESS_TOKEN;
    this.initClient();
  }

  (0, _createClass2["default"])(LineHelper, [{
    key: "initClient",
    value: function initClient() {
      this.client = new line.Client({
        channelAccessToken: this.channelAccessToken
      });
    }
  }, {
    key: "webHookHOC",
    value: function webHookHOC(options) {
      var _this = this;

      return /*#__PURE__*/function () {
        var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(payload) {
          var promiseArr;
          return _regenerator["default"].wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  console.log(JSON.stringify(payload)); // TODO: Demo

                  if (!(payload.events.length > 0)) {
                    _context.next = 12;
                    break;
                  }

                  promiseArr = [];
                  payload.events.forEach(function (v) {
                    var userId = v.source.userId;
                    var replyToken = v.replyToken;

                    var getProfile = _this.getProfile(userId);

                    if (v.type === "message" && options.messageReply) {
                      promiseArr = options.messageReply(v, v.message.type, v.message.text, userId, replyToken, getProfile);
                    } else if (v.type === "beacon" && options.beaconReply) {
                      var _v$beacon = v.beacon,
                          hwid = _v$beacon.hwid,
                          dm = _v$beacon.dm,
                          type = _v$beacon.type;
                      promiseArr = options.beaconReply(v, hwid, dm, type, userId, replyToken, getProfile);
                    } else if (v.type === "follow" && options.followReply) {
                      promiseArr = options.followReply(v, userId, replyToken, getProfile);
                    } else if (v.type === "unfollow" && options.unFollowReply) {
                      promiseArr = options.unFollowReply(v);
                    } else if (v.type === "join" && options.joinReply) {
                      promiseArr = options.joinReply(v);
                    } else if (v.type === "leave" && options.leaveReply) {
                      promiseArr = options.leaveReply(v);
                    } else if (v.type === "memberJoined" && options.memberJoinReply) {
                      promiseArr = options.memberJoinReply(v, v.joined);
                    } else if (v.type === "memberLeft" && options.memberLeftReply) {
                      promiseArr = options.memberLeftReply(v, v.left);
                    } else if (v.type === "postback" && options.postBackReply) {
                      promiseArr = options.postBackReply(v, replyToken, getProfile, v.postback);
                    }
                  });
                  _context.prev = 4;
                  _context.next = 7;
                  return Promise.all(promiseArr);

                case 7:
                  _context.next = 12;
                  break;

                case 9:
                  _context.prev = 9;
                  _context.t0 = _context["catch"](4);
                  (0, _lineNotify.errorNotify)("yellow-idea-sdk >> line-bot.js >> webHookHOC", "".concat(JSON.stringify(payload), "\n").concat(_context.t0));

                case 12:
                  return _context.abrupt("return", null);

                case 13:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, null, [[4, 9]]);
        }));

        return function (_x) {
          return _ref.apply(this, arguments);
        };
      }();
    }
  }, {
    key: "pushMessage",
    value: function () {
      var _pushMessage = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(message, user) {
        var _this2 = this;

        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (!(typeof user === "string")) {
                  _context2.next = 4;
                  break;
                }

                return _context2.abrupt("return", new Promise(function (resolve, reject) {
                  _this2.client.pushMessage(user, message).then(function () {
                    resolve(true);
                  })["catch"](function (err) {
                    var jsonMsg = JSON.stringify(message);
                    (0, _lineNotify.errorNotify)("yellow-idea-sdk >> line-bot.js >> pushMessage", "".concat(jsonMsg, "\n").concat(user, "\n").concat(err));
                    reject(err);
                  });
                }));

              case 4:
                return _context2.abrupt("return", new Promise(function (resolve, reject) {
                  _this2.client.multicast(user, message).then(function () {
                    resolve(true);
                  })["catch"](function (err) {
                    var jsonMsg = JSON.stringify(message);
                    (0, _lineNotify.errorNotify)("yellow-idea-sdk >> line-bot.js >> pushMessage", "".concat(jsonMsg, "\n").concat(user, "\n").concat(err));
                    reject(err);
                  });
                }));

              case 5:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function pushMessage(_x2, _x3) {
        return _pushMessage.apply(this, arguments);
      }

      return pushMessage;
    }()
  }, {
    key: "replyMessage",
    value: function () {
      var _replyMessage = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(message, token) {
        var _this3 = this;

        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                return _context3.abrupt("return", new Promise(function (resolve, reject) {
                  _this3.client.replyMessage(token, message).then(function () {
                    resolve(true);
                  })["catch"](function (err) {
                    var jsonMsg = JSON.stringify(message);
                    (0, _lineNotify.errorNotify)("yellow-idea-sdk >> line-bot.js >> replyMessage", "".concat(jsonMsg, "\n").concat(token, "\n").concat(err));
                    reject(err);
                  });
                }));

              case 1:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function replyMessage(_x4, _x5) {
        return _replyMessage.apply(this, arguments);
      }

      return replyMessage;
    }()
  }, {
    key: "getProfile",
    value: function getProfile(userId) {
      var _this4 = this;

      return new Promise(function (resolve, reject) {
        _this4.client.getProfile(userId).then(function (res) {
          resolve(res);
        })["catch"](function (err) {
          (0, _lineNotify.errorNotify)("yellow-idea-sdk >> line-bot.js >> getProfile", "".concat(userId, "\n").concat(err));
          reject(null);
        });
      });
    }
  }]);
  return LineHelper;
}();

var _default = new LineHelper();

exports["default"] = _default;