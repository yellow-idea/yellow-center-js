"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.errorNotify = errorNotify;

var request = require("request");

function errorNotify(fn, msg) {
  var message = "\nProject  : ".concat(process.env.LINE_NOTIFY_PROJECT_NAME || "????", "\nFunction : ").concat(fn, "\n====================================\n").concat(msg);
  var options = {
    method: "POST",
    url: "https://notify-api.line.me/api/notify",
    headers: {
      Accept: "*/*",
      Authorization: "Bearer ".concat(process.env.LINE_NOTIFY_TOKEN || "oM14UdZdc5nhJbKzA2a84XcGhtUyT0A3yTwFqyKRD9B"),
      "Content-Type": "application/x-www-form-urlencoded"
    },
    form: {
      message: message
    }
  };
  console.log(message);
  request(options, function (error, response, body) {
    if (error) {
      console.log(error);
    }
  });
}