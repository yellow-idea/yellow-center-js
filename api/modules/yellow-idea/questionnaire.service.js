"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.list = list;
exports.show = show;
exports.create = create;
exports.importData = importData;
exports.update = update;
exports.softDelete = softDelete;
exports.toggleActive = toggleActive;
exports.appShowSubmit = appShowSubmit;
exports.appSubmit = appSubmit;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _mongodb = require("mongodb");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function list(payload) {
  var filter = {
    deleted_at: null
  };
  return this.repository.list(_objectSpread({}, filter), 10, 0, "created_at", -1);
}

function show(_id) {
  _id = (0, _mongodb.ObjectID)(_id);
  var filter = {
    _id: _id
  };
  return this.repository.show(_objectSpread({}, filter));
}

function create(_x) {
  return _create.apply(this, arguments);
}

function _create() {
  _create = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(payload) {
    var $count, created_at;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            delete payload.id;
            delete payload._id;
            _context.next = 4;
            return this.repository.count({
              name: payload.name
            });

          case 4:
            $count = _context.sent;

            if (!($count > 0)) {
              _context.next = 7;
              break;
            }

            return _context.abrupt("return", {
              status_code: "DUPLICATE_DATA"
            });

          case 7:
            created_at = new Date();
            return _context.abrupt("return", this.repository.create(_objectSpread(_objectSpread({
              is_active: false
            }, payload), {}, {
              created_at: created_at,
              updated_at: created_at,
              deleted_at: null
            })));

          case 9:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));
  return _create.apply(this, arguments);
}

function importData(_x2) {
  return _importData.apply(this, arguments);
}

function _importData() {
  _importData = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(payload) {
    var created_at;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            created_at = new Date();
            return _context2.abrupt("return", this.repository.importData(payload.map(function (v) {
              return _objectSpread(_objectSpread({}, v), {}, {
                created_at: created_at,
                updated_at: created_at,
                deleted_at: null
              });
            })));

          case 2:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }));
  return _importData.apply(this, arguments);
}

function update(_x3, _x4) {
  return _update.apply(this, arguments);
}

function _update() {
  _update = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(_id, payload) {
    var $count;
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _id = (0, _mongodb.ObjectID)(_id);
            delete payload._id;
            delete payload.id;
            _context3.next = 5;
            return this.repository.count({
              name: payload.name,
              _id: {
                $ne: _id
              }
            });

          case 5:
            $count = _context3.sent;

            if (!($count > 0)) {
              _context3.next = 8;
              break;
            }

            return _context3.abrupt("return", {
              status_code: "DUPLICATE_DATA"
            });

          case 8:
            return _context3.abrupt("return", this.repository.update({
              _id: _id
            }, _objectSpread(_objectSpread({}, payload), {}, {
              updated_at: new Date()
            })));

          case 9:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, this);
  }));
  return _update.apply(this, arguments);
}

function softDelete(_x5) {
  return _softDelete.apply(this, arguments);
}

function _softDelete() {
  _softDelete = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(_id) {
    var $data;
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _id = (0, _mongodb.ObjectID)(_id);
            _context4.next = 3;
            return this.repository.show({
              _id: _id
            }, {
              is_active: 1
            });

          case 3:
            $data = _context4.sent;

            if ($data) {
              _context4.next = 6;
              break;
            }

            return _context4.abrupt("return", {
              status_code: "NO_DATA"
            });

          case 6:
            return _context4.abrupt("return", this.repository["delete"]({
              _id: _id
            }));

          case 7:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, this);
  }));
  return _softDelete.apply(this, arguments);
}

function toggleActive(_x6) {
  return _toggleActive.apply(this, arguments);
}

function _toggleActive() {
  _toggleActive = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(_id) {
    var $data;
    return _regenerator["default"].wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _id = (0, _mongodb.ObjectID)(_id);
            _context5.next = 3;
            return this.repository.show({
              _id: _id
            }, {
              is_active: 1
            });

          case 3:
            $data = _context5.sent;

            if ($data) {
              _context5.next = 6;
              break;
            }

            return _context5.abrupt("return", {
              status_code: "NO_DATA"
            });

          case 6:
            return _context5.abrupt("return", this.repository.update({
              _id: _id
            }, {
              is_active: !$data.is_active,
              updated_at: new Date()
            }));

          case 7:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, this);
  }));
  return _toggleActive.apply(this, arguments);
}

function appShowSubmit(payload) {
  var filter = {
    _id: (0, _mongodb.ObjectID)(payload.id)
  };
  console.log(_objectSpread({}, filter));
  return this.repository.appShowSubmit(_objectSpread({}, filter));
}

function appSubmit(payload) {
  var created_at = new Date();
  return this.repository.appSubmit(_objectSpread(_objectSpread({}, payload), {}, {
    created_at: created_at,
    updated_at: created_at
  }));
}