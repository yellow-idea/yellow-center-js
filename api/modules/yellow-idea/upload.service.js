"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.uploadCDNService = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _uuid = require("uuid");

var _upload = require("../../helper/upload");

var uploadCDNService = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
    var payload, ext, data;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            payload = req.body;
            payload.path = payload.path ? payload.path : "";
            ext = (0, _upload.GetFileExtentions)(payload.img_url);
            _context.next = 5;
            return (0, _upload.S3UploadCDN)(payload.img_url, "images" + payload.path + "/" + (0, _uuid.v1)() + "." + ext);

          case 5:
            data = _context.sent;
            res.json({
              data: data
            });

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function uploadCDNService(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.uploadCDNService = uploadCDNService;