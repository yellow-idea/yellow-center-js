import { ObjectId } from "mongodb"
import { MongoConnection } from "../src/helper/mongo"
import QuestionnaireService from "../src/modules/yellow-idea/questionnaire.service"

(async function() {
    const client = await MongoConnection("mongodb://admin:12345678@localhost:27017/?authSource=admin")
    const service = new QuestionnaireService({ db : client.db("Demo2") })
    /* Create */
    // const response = await service.create({ name : "Q1xxx" })

    /* Update */
    // const response = await service.update("5f1169b910fe9304b09cd20c" , {
    //     _id : "5f1169b910fe9304b09cd20c" ,
    //     name : "Q19999"
    // })

    /* Delete */
    // const response = await service.delete(ObjectId("5f119615552a11067762aea3"))

    /* List */
    // const response = await service.list({})

    /* Show */
    // const response = await service.show(ObjectId("5f119615552a11067762aea3"))

    client.close()
    // console.log(response)
})()
