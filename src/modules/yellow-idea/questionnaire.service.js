import { ObjectID } from "mongodb"

export function list(payload) {
    const filter = { deleted_at : null }
    return this.repository.list({ ...filter } , 10 , 0 , "created_at" , -1)
}

export function show(_id) {
    _id = ObjectID(_id)
    const filter = { _id }
    return this.repository.show({ ...filter })
}

export async function create(payload) {
    delete payload.id
    delete payload._id
    const $count = await this.repository.count({ name : payload.name })
    if( $count > 0 ) {
        return { status_code : "DUPLICATE_DATA" }
    }
    const created_at = new Date()
    return this.repository.create({
        is_active : false ,
        ...payload ,
        created_at ,
        updated_at : created_at ,
        deleted_at : null
    })
}

export async function importData(payload) {
    const created_at = new Date()
    return this.repository.importData(payload.map(v => {
        return {
            ...v ,
            created_at ,
            updated_at : created_at ,
            deleted_at : null
        }
    }))
}

export async function update(_id , payload) {
    _id = ObjectID(_id)
    delete payload._id
    delete payload.id
    const $count = await this.repository.count({ name : payload.name , _id : { $ne : _id } })
    if( $count > 0 ) {
        return { status_code : "DUPLICATE_DATA" }
    }
    return this.repository.update({ _id } , {
        ...payload ,
        updated_at : new Date()
    })
}

export async function softDelete(_id) {
    _id = ObjectID(_id)
    const $data = await this.repository.show({ _id } , { is_active : 1 })
    if( !$data ) {
        return { status_code : "NO_DATA" }
    }
    return this.repository.delete({ _id })
}

export async function toggleActive(_id) {
    _id = ObjectID(_id)
    const $data = await this.repository.show({ _id } , { is_active : 1 })
    if( !$data ) {
        return { status_code : "NO_DATA" }
    }
    return this.repository.update({ _id } , {
        is_active : !$data.is_active ,
        updated_at : new Date()
    })
}

export function appShowSubmit(payload) {
    const filter = { _id : ObjectID(payload.id) }
    console.log({ ...filter })
    return this.repository.appShowSubmit({ ...filter })
}

export function appSubmit(payload) {
    const created_at = new Date()
    return this.repository.appSubmit({
        ...payload ,
        created_at ,
        updated_at : created_at
    })
}
