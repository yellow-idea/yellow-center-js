import { v1 as uv1 } from "uuid"
import { GetFileExtentions , S3UploadCDN } from "../../helper/upload"

export const uploadCDNService = async(req , res) => {
    const payload = req.body
    payload.path = payload.path ? payload.path : ""
    const ext = GetFileExtentions(payload.img_url)
    const data = await S3UploadCDN(payload.img_url , "images" + payload.path + "/" + uv1() + "." + ext)
    res.json({ data })
}
