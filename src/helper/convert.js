export const replaceID = obj => JSON.parse(JSON.stringify(obj).replace(/"_id"/g , `"id"`))
export const convert_array_to_join_str = (arr , separator , key , sq = false) => {
    let tmp_arr = []
    let result = ""
    if( arr.length > 0 ) {
        arr.forEach(v => {
            if( sq ) {
                tmp_arr.push(`'${ v[key] }'`)
            } else {
                tmp_arr.push(v[key])
            }
        })
        result = tmp_arr.join(separator)
    }
    return result
}
