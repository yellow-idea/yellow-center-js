import fs from 'fs';
import S3 from 'aws-sdk/clients/s3';

export const GetFileExtentions = img64 => {
    const data = img64.match(/^data:image\/([a-zA-Z]*);base64,/);
    return data[1];
};

export const UploadImageBase64 = (img64 = "", fileName = "") => new Promise((resolve => {
    const base64Data = img64.replace(/^data:image\/([a-zA-Z]*);base64,/, "");
    fs.writeFile(fileName, base64Data, 'base64', function (err) {
        if (err) {
            console.log(err);
            resolve(false);
        } else {
            resolve(true);
        }
    });
}));

export const RemoveFile = (fileName = "") => new Promise(resolve => {
    fs.unlink(fileName, function (err) {
        if (err) {
            console.log(err);
            resolve(false);
        } else {
            resolve(true);
        }
    });
});

export const S3Uploader = options => new Promise(async resolve => {
    if (options.img_url === "" || options.img_url.search("http") > -1) {
        resolve(options.img_url);
    } else {
        const ext = GetFileExtentions(options.img_url);
        const fileName = `${options.fileName}.${ext}`;
        const tmpFile = `/tmp/${fileName}`;
        const resUpload = await UploadImageBase64(options.img_url, tmpFile);
        if (resUpload) {
            const locationUrl = await S3Upload(options.bucket, `${options.key}/${fileName}`, tmpFile);
            await RemoveFile(tmpFile);
            resolve(locationUrl);
        } else {
            resolve(null);
        }
    }
});

export const S3Upload = (bucket, key, tmpFile) => new Promise(resolve => {
    const s3 = new S3({
        region: process.env.SERVICE_REGION,
        accessKeyId: process.env.ACCESS_KEY_ID,
        secretAccessKey: process.env.SECRET_ACCESS_KEY
    });
    const fileStream = fs.createReadStream(tmpFile);
    fileStream.on('error', function (err) {
        console.log('File Error', err);
        resolve(null);
    });
    const params = {
        Bucket: bucket,
        Key: key,
        Body: fileStream
    };
    s3.putObject(params, function (err, data) {
        if (err) {
            console.log(err, data);
            resolve(null);
        } else {
            resolve(`https://s3-${process.env.SERVICE_REGION}.amazonaws.com/${bucket}/${key}`);
        }
    });
});


export const easyS3Upload = (key, img_url, fileName) => new Promise(async resolve => {
    const img_data = await S3Uploader({
        bucket: process.env.S3,
        key,
        img_url,
        fileName
    });
    resolve(img_data);
});

export const S3UploadCDN = (imgBase64, key) => new Promise(resolve => {
    let a1 = `01000001 01001011 01001001 01000001 01010010 00110101 01000100 00110111 01010000 01010010 00110111 01000100 01010111 00110110 00110110 01011010 01010001 01001101 00110110 01010100`
    let a2 = `01110010 00111000 01111010 01101100 01000111 00111001 01001100 01010011 01110100 01010111 01110010 01101001 01010010 01101001 00110000 01110001 01000101 01011000 01011010 00110101 01101000 01011001 01000101 01000101 01000101 01000110 00110100 01101010 01010000 01101011 01010011 01100111 01100010 01010110 00110110 01101001 01101011 01010100 01000010 01111010 `
    let accessKeyId = a1.split(" ").map(bin => String.fromCharCode(parseInt(bin , 2))).join("")
    let secretAccessKey = a2.split(" ").map(bin => String.fromCharCode(parseInt(bin , 2))).join("")
    const s3 = new S3({
        region: "ap-southeast-1",
        accessKeyId : accessKeyId ,
        secretAccessKey : secretAccessKey
    });
    const buf = new Buffer(imgBase64.replace(/^data:image\/\w+;base64,/, ""), 'base64');
    const params = {Bucket: "cdn.yellow-idea.com", Key: key, Body: buf};
    s3.putObject(params, function (err, data) {
        if (err) {
            console.log(err, data);
            resolve(null);
        } else {
            resolve(`https://cdn.yellow-idea.com/${key}`);
        }
    });
});
