import { errorNotify } from "./line-notify"

export async function list(filter = {} , projects = {} , limit = 10 , offset = 0 , sort = "" , order = "") {
    try {
        return {
            rows : await this.collection.find(filter , projects).skip(parseInt(offset.toString())).limit(limit).toArray() ,
            total : await this.count(filter)
        }
    } catch( e ) {
        errorNotify(`${ this.className } >> list` , `${ e }\n${ JSON.stringify({filter,offset,limit}) }`)
        return { rows : [] , total : 0 }
    }
}

export async function array(filter = {} , projects = {}) {
    const data = await this.collection.find(filter , projects).toArray()
    try {
        return {
            data : data
        }
    } catch( e ) {
        errorNotify(`${ this.className } >> array` , `${ e }\n${ JSON.stringify(filter) }`)
        return { data : [] }
    }
}

export async function show(filter = {} , projects = {}) {
    const data = await this.collection.find(filter , projects).toArray()
    try {
        return {
            data : data.length > 0 ? data[0] : null
        }
    } catch( e ) {
        errorNotify(`${ this.className } >> show` , `${ e }\n${ JSON.stringify(filter) }`)
        return { data : null }
    }
}

export async function count(filter = {}) {
    return this.collection.countDocuments(filter)
}

export async function create(payload) {
    try {
        const result = await this.collection.insertOne(payload)
        return { status_code : "SUCCESS" , id : result.insertedId }
    } catch( e ) {
        errorNotify(`${ this.className } >> create` , `${ e }\n${ JSON.stringify(payload) }`)
        return { status_code : "ERROR" }
    }
}

export async function update(filter = { id : null } , payload) {
    try {
        const result = await this.collection.updateOne(filter , { $set : { ...payload } })
        if( result.matchedCount <= 0 ) {
            return { status_code : "NO_DATA" }
        }
        return { status_code : "SUCCESS" }
    } catch( e ) {
        errorNotify(`${ this.className } >> update` , `${ e }\n${ JSON.stringify(filter) }\n${ JSON.stringify(payload) }`)
        return { status_code : "ERROR" }
    }
}

export async function softDelete(filter = { _id : null }) {
    try {
        const result = await this.collection.findOneAndUpdate(filter , { $set : { deleted_at : new Date() } })
        if( result.matchedCount <= 0 ) {
            return { status_code : "NO_DATA" }
        }
        return { status_code : "SUCCESS" }
    } catch( e ) {
        errorNotify(`${ this.className } >> softDelete` , `${ e }\n${ JSON.stringify(filter) }`)
        return { status_code : "ERROR" }
    }
}

export async function importData(payload) {
    try {
        await this.collection.insertMany(payload)
        return { status_code : "SUCCESS" }
    } catch( e ) {
        errorNotify(`${ this.className } >> importData` , `${ e }\n${ JSON.stringify(payload) }`)
        return { status_code : "ERROR" }
    }
}

export async function appShowSubmit(filter = {} , projects = {}) {
    const data = await this.collection_app.find(filter , projects).toArray()
    try {
        return {
            data : data.length > 0 ? data[0] : null
        }
    } catch( e ) {
        errorNotify(`${ this.className } >> appShowSubmit` , `${ e }\n${ JSON.stringify(filter) }`)
        return { data : null }
    }
}

export async function appSubmit(payload) {
    try {
        const result = await this.collection_app.insertOne(payload)
        return { status_code : "SUCCESS" , id : result.insertedId }
    } catch( e ) {
        errorNotify(`${ this.className } >> appSubmit` , `${ e }\n${ JSON.stringify(payload) }`)
        return { status_code : "ERROR" }
    }
}
