export const isArray = value => value && typeof value === 'object' && value.constructor === Array;
export const isObject = value => value && typeof value === 'object' && value.constructor === Object;
export const isHasKey = (obj, key) => Object.keys(obj).find(x => x === key);
export const isNotNullAndNotEmpty = obj => obj ? obj !== "" : false;
