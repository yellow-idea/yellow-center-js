import {errorNotify} from './line-notify';

const line = require('@line/bot-sdk');

class LineHelper {
    constructor() {
        this.channelAccessToken = process.env.LINE_CHANEL_ACCESS_TOKEN;
        this.initClient();
    }

    initClient() {
        this.client = new line.Client({
            channelAccessToken: this.channelAccessToken
        });
    }

    webHookHOC(options) {
        return async payload => {
            console.log(JSON.stringify(payload)); // TODO: Demo
            if (payload.events.length > 0) {
                let promiseArr = [];
                payload.events.forEach(v => {
                    const userId = v.source.userId;
                    const replyToken = v.replyToken;
                    const getProfile = this.getProfile(userId);
                    if (v.type === "message" && options.messageReply) {
                        promiseArr = options.messageReply(v, v.message.type, v.message.text, userId, replyToken, getProfile);
                    } else if (v.type === "beacon" && options.beaconReply) {
                        const {hwid, dm, type} = v.beacon;
                        promiseArr = options.beaconReply(v, hwid, dm, type, userId, replyToken, getProfile);
                    } else if (v.type === "follow" && options.followReply) {
                        promiseArr = options.followReply(v, userId, replyToken, getProfile);
                    } else if (v.type === "unfollow" && options.unFollowReply) {
                        promiseArr = options.unFollowReply(v);
                    } else if (v.type === "join" && options.joinReply) {
                        promiseArr = options.joinReply(v);
                    } else if (v.type === "leave" && options.leaveReply) {
                        promiseArr = options.leaveReply(v);
                    } else if (v.type === "memberJoined" && options.memberJoinReply) {
                        promiseArr = options.memberJoinReply(v, v.joined);
                    } else if (v.type === "memberLeft" && options.memberLeftReply) {
                        promiseArr = options.memberLeftReply(v, v.left);
                    } else if (v.type === "postback" && options.postBackReply) {
                        promiseArr = options.postBackReply(v, replyToken, getProfile, v.postback);
                    }
                });
                try {
                    await Promise.all(promiseArr);
                } catch (e) {
                    errorNotify("yellow-idea-sdk >> line-bot.js >> webHookHOC", `${JSON.stringify(payload)}\n${e}`);
                }
            }
            return null;
        };
    }

    async pushMessage(message, user) {
        if (typeof user === "string") {
            return new Promise((resolve, reject) => {
                this.client.pushMessage(user, message)
                    .then(() => {
                        resolve(true)
                    })
                    .catch((err) => {
                        const jsonMsg = JSON.stringify(message);
                        errorNotify("yellow-idea-sdk >> line-bot.js >> pushMessage", `${jsonMsg}\n${user}\n${err}`);
                        reject(err)
                    });
            })
        } else {
            return new Promise((resolve, reject) => {
                this.client.multicast(user, message)
                    .then(() => {
                        resolve(true)
                    })
                    .catch((err) => {
                        const jsonMsg = JSON.stringify(message);
                        errorNotify("yellow-idea-sdk >> line-bot.js >> pushMessage", `${jsonMsg}\n${user}\n${err}`);
                        reject(err)
                    });
            })
        }
    }

    async replyMessage(message, token) {
        // errorNotify("`กำลัง Reply Message`", `${JSON.stringify(message)}`);
        return new Promise((resolve, reject) => {
            this.client.replyMessage(token, message)
                .then(() => {
                    resolve(true)
                })
                .catch((err) => {
                    const jsonMsg = JSON.stringify(message);
                    errorNotify("yellow-idea-sdk >> line-bot.js >> replyMessage", `${jsonMsg}\n${token}\n${err}`);
                    reject(err)
                });
        })
    }

    getProfile(userId) {
        return new Promise((resolve, reject) => {
            this.client.getProfile(userId)
                .then(res => {
                    resolve(res)
                })
                .catch((err) => {
                    errorNotify("yellow-idea-sdk >> line-bot.js >> getProfile", `${userId}\n${err}`);
                    reject(null)
                });
        })
    }
}

export default new LineHelper();
