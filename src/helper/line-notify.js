const request = require("request")

export function errorNotify(fn , msg) {
    const message = `
Project  : ${ process.env.LINE_NOTIFY_PROJECT_NAME || "????" }
Function : ${ fn }
====================================
${ msg }`
    const options = {
        method : "POST" ,
        url : "https://notify-api.line.me/api/notify" ,
        headers :
            {
                Accept : "*/*" ,
                Authorization : `Bearer ${ process.env.LINE_NOTIFY_TOKEN || "oM14UdZdc5nhJbKzA2a84XcGhtUyT0A3yTwFqyKRD9B" }` ,
                "Content-Type" : "application/x-www-form-urlencoded"
            } ,
        form : { message }
    }
    console.log(message)
    request(options , function(error , response , body) {
        if( error ) {
            console.log(error)
        }
    })
}
