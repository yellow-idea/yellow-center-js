const MongoClient = require("mongodb").MongoClient

export const MongoConnection = async url => {
    return new Promise((resolve , reject) => {
        MongoClient.connect(url , {
            useNewUrlParser : true ,
            useUnifiedTopology : true
        } , (err , client) => {
            if( err ) {
                reject(err)
            }
            console.log("Connected successfully to server")
            resolve(client)
        })
    })
}
